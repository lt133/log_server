#!/usr/bin/env python
# -*- coding:utf-8 -*-

from __future__ import print_function
import logging, logging.handlers
from logging.handlers import SocketHandler
import time
import tail

logLevel = logging.INFO

logger_sms = logging.getLogger('sms')
logger = logging.getLogger('sms.sgip1')

handler = SocketHandler('localhost', 9030)
datefmt = "%Y-%m-%d %H:%M:%S"
format_str = "[%(asctime)s]: %(levelname)s %(message)s"
formatter = logging.Formatter(format_str, datefmt)
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logLevel)


def log_line(txt):
    ''' Prints received text '''
    print(txt, end='')
    logger.info(txt.replace('\n', '').replace('\r', ''))


t = tail.Tail('/tmp/test.log')
t.register_callback(log_line)
t.follow(s=5)



