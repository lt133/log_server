import re

pattern = re.compile(r'hello')

match = pattern.match(r'hello1 world! hello2')

if match:
    print match.group(), match.group(0)
    print match.groups()
    print match.groupdict()
