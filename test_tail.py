#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
python-tail example.
Does a tail follow against /var/log/syslog with a time interval of 5 seconds.
Prints recieved new lines to standard out '''

from __future__ import print_function
import tail


def print_line(txt):
    ''' Prints received text '''
    print(txt, end='')


t = tail.Tail('/tmp/test.log')
t.register_callback(print_line)
t.follow(s=5)
